from PIL import Image
import os
from os import path

# should be 4:3
# check if width or height is longer
# compute the other side of the longer side
# ex. if width is larger, compute height = 3/4 width

def black_bg(image):
    (x, y) = image.size
    if x > 4 * y / 3:
        y_temp = y
        y = int(3 * x / 4)
        shift = (y - y_temp)/2
        axis = "y"
    else:
        x_temp = x
        x = int(4 * y / 3)
        shift = (x - x_temp)/2
        axis = "x"

    layer = Image.new('RGB', (x, y), (0, 0, 0))
    layer.paste(image, tuple(map(lambda x: int((x[0] - x[1]) / 2), zip((x, y), image.size))))
    return layer, axis, shift


print("Image resizer working..")
# check CSV then rename those files to arrange names to images/00000000.jpg and so on
# open csv, then loop per column
# rename using rename() function, save int for the filename count, make sure filename is 8 digits.jpg
# also change the value in the csv file

### CONFIGS
f = open("face_train_new.csv", "r")
out_file = open("resized_train.csv", "w+")
src_path = "WIDER_train/images/"

content = f.read()

start = 0
exist = 0  # counts how many is renamed
total = 0
out_fn = []
end = len(content) - 1  # total num of characters
temp_end = end
cont = 1

prev_src = ""
prev_x = -1
prev_y = -1

final_count = 23

out_file.write("filename,xmin,ymin,xmax,ymax\n")

while cont == 1:
    # if count == final_count+1:
    #         cont = 0

    start = content.find("\n", start, end) + 1
    if start == 0:
        print("FINISHED ALL FILES!")
        print("Total exist: " + str(exist))
        print("Total: " + str(total))
        break

    if content.find(".jpg", start, end) != -1:
        temp_end = content.find(".jpg", start, end) + 4
        filename = content[start:temp_end]

        start = content.find(",", start, end) + 1
        temp_end = content.find(",", start, end)
        x_min = int(content[start:temp_end])

        start = content.find(",", start, end) + 1
        temp_end = content.find(",", start, end)
        y_min = int(content[start:temp_end])

        start = content.find(",", start, end) + 1
        temp_end = content.find(",", start, end)
        x_max = int(content[start:temp_end])

        start = content.find(",", start, end) + 1
        temp_end = content.find("\n", start, end)
        y_max = int(content[start:temp_end])

        src = src_path + filename

        total += 1

        if path.exists(src):

            # counts all existing
            exist += 1

            # for new image
            if prev_src != src:

                print("total: " + str(total) + ", existing: " + str(exist))
                img = Image.open(src)

                # axis, can be x or y, shift is amount of shift added / 2
                final, axis, shift = black_bg(img)

                # width and height
                (width, height) = final.size

                # ratio of new to shifted size
                ratio = 640/width

                # shifting bounding box
                if axis == "x":
                    x_min += shift
                    x_max += shift
                else:
                    y_min += shift
                    y_max += shift

                # scaling bounding box
                x_min *= ratio
                x_max *= ratio
                y_min *= ratio
                y_max *= ratio

                final = final.resize((640, 480), Image.ANTIALIAS)
                final.save(src)

                out_file.write(src + "," + str(int(x_min)) + "," + str(int(y_min)) + "," + str(int(x_max)) + "," + str(int(y_max)) + "\n")

                # final = final.crop((x_min, y_min, x_max, y_max))
                # final.show()

                # Saving previous data
                prev_src = src

            # for repeated image
            else:
                img = Image.open(src)

                # shifting bounding box
                if axis == "x":
                    x_min += shift
                    x_max += shift
                else:
                    y_min += shift
                    y_max += shift

                # scaling bounding box
                x_min *= ratio
                x_max *= ratio
                y_min *= ratio
                y_max *= ratio

                # img = img.crop((x_min, y_min, x_max, y_max))
                # works properly

                # saving new bounding box to csv
                out_file.write(src + "," + str(int(x_min)) + "," + str(int(y_min)) + "," + str(int(x_max)) + "," + str(int(y_max)) + "\n")
        else:
            print("path: " + src + " doesn't exist.")
