print("WIDER FACE PARSER")
#replace desired input and output files

#input
f = open("wider_face_split/wider_face_val_bbx_gt.txt", "r")
content = f.read()

#output
new_csv = open("face_val_new.csv", "w+")

start = 0
count = 0 #counts how many is parsed
temp_out = []
end = len(content) - 1 #total num of characters
cont = 1

temp_out.append("filename,xmin,ymin,xmax,ymax\n")

while cont == 1:
	if content.find(".jpg", start, end) != -1:
		#filename
		temp_end = content.find(".jpg", start, end) + 4
		filename = content[start: temp_end]
		temp_out.append(filename) #works
		temp_out.append(",")

		start = temp_end + 1

		#num of bounding box
		temp_end = content.find("\n", start, end)
		#print("# of bbx: ")
		#print(content[start: temp_end])

		# +1 avoids the \n
		start = temp_end + 1

		#xmin
		temp_end = content.find(" ", start, end)
		temp_out.append(content[start: temp_end])
		temp_out.append(",")
		xmin = int(content[start: temp_end], 10)

		# +1 avoids the " "
		start = temp_end + 1

		#ymin
		temp_end = content.find(" ", start, end)
		temp_out.append(content[start: temp_end])
		temp_out.append(",")
		ymin = int(content[start: temp_end], 10)

		# +1 avoids the " "
		start = temp_end + 1

		#xmax
		temp_end = content.find(" ", start, end)
		xmax = xmin + int(content[start: temp_end],10)
		temp_out.append(str(xmax))
		temp_out.append(",")

		# +1 avoids the " "
		start = temp_end + 1

		#ymax
		temp_end = content.find(" ", start, end)
		ymax = ymin + int(content[start: temp_end],10)
		temp_out.append(str(ymax))

		temp_out.append("\n")

		#0--Parade/0_Parade_marchingband_1_465.jpg
		#2
		#345 211 4 4 2 0 0 0 2 0
		#331 126 3 3 0 0 0 1 0 0

		#for multiple bounding box
		t_start = content.find("\n", start, end) #next coordinates
		t_end = content.find("--", start, end) - 3
		if str.isdigit(content[t_end-1]):
			t_end -= 1

		if t_end == -1:
			t_end = end

		#check if there are still new lines
		while content.find("\n", t_start, t_end) != -1:
			t_start += 1

			#filename
			temp_out.append(filename)
			temp_out.append(",")

			#xmin
			temp_end = content.find(" ", t_start, t_end)
			temp_out.append(content[t_start: temp_end])
			#print("xmin: ", content[t_start: temp_end])
			temp_out.append(",")
			xmin = int(content[t_start: temp_end], 10)

			# +1 avoids the " "
			t_start = temp_end + 1

			#ymin
			temp_end = content.find(" ", t_start, t_end)
			temp_out.append(content[t_start: temp_end])
			temp_out.append(",")
			ymin = int(content[t_start: temp_end], 10)

			# +1 avoids the " "
			t_start = temp_end + 1

			#xmax
			temp_end = content.find(" ", t_start, t_end)
			xmax = xmin + int(content[t_start: temp_end],10)
			temp_out.append(str(xmax))
			temp_out.append(",")

			# +1 avoids the " "
			t_start = temp_end + 1

			#ymax
			temp_end = content.find(" ", t_start, t_end)
			ymax = ymin + int(content[t_start: temp_end],10)
			temp_out.append(str(ymax))

			temp_out.append("\n")

			t_start = content.find("\n", t_start, t_end)
			#print("t_start: ", t_start)
			#print(content[t_start: t_end])

		#finds start of next image
		start = content.find("--", start, end) - 1
		if str.isdigit(content[start-1]):
			start -= 1

		#WRITE TO FILE OUTPUT
		new_csv.write(''.join(temp_out))

		del temp_out[:]
	else:
		cont = 0

print("done parsing!")

f.close()
new_csv.close()
