import matplotlib.pyplot as plt
import matplotlib.patches as patches
from PIL import Image
import numpy as np
import csv
from shutil import copyfile

OUTPUT_TRAIN_IMG = "E:\\WIDER FACE\\train_imgs\\"

def showImgAndBBox():
    im = np.array(Image.open('train_imgs/00011559.png'), dtype=np.uint8)

    # Create figure and axes
    fig,ax = plt.subplots(1)

    # Display the image
    ax.imshow(im)
    # 376,451,132,217
    xmin = 376
    xmax = 451
    ymin = 132
    ymax = 217

    # Create a Rectangle patch
    rect = patches.Rectangle((xmin,ymin),xmax-xmin,ymax-ymin,linewidth=1,edgecolor='r',facecolor='none')

    # Add the patch to the Axes
    ax.add_patch(rect)

    plt.show()

def mergeFDDBAndWIDERFaces():
    ### WIDER FACES
    currentImgNum = 15724
    prevImgTrain = ""
    csvData_train = [['imgnum', 'xmin', 'xmax', 'ymin', 'ymax', 'hasHead']]
    with open('E:\\WIDER FACE\\resized_val.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                pass
            else:
                subdir = row[0]
                xmin = int(float(row[1]))
                ymin = int(float(row[2]))
                xmax = int(float(row[3]))
                ymax = int(float(row[4]))
                if prevImgTrain != subdir:
                    currentImgNum += 1
                    prevImgTrain = subdir
                    origImgFile = "E:/WIDER FACE/" + subdir
                    outputImgFile = OUTPUT_TRAIN_IMG + str(currentImgNum).zfill(8) + ".png"
                    # copyfile(origImgFile, outputImgFile)
                    im = Image.open(origImgFile)
                    im.save(outputImgFile)
                    rowdata = [str(currentImgNum).zfill(8) + ".png", str(xmin), str(xmax), str(ymin), str(ymax), str(1)]
                    print(rowdata)
                    csvData_train.append(rowdata)
                else:
                    rowdata = [str(currentImgNum).zfill(8) + ".png", str(xmin), str(xmax), str(ymin), str(ymax), str(1)]
                    print(rowdata)
                    csvData_train.append(rowdata)
            line_count += 1
    prevImgTrain = ""
    # with open('E:\\WIDER FACE\\resized_train_fddb.csv') as csv_file:
    #     csv_reader = csv.reader(csv_file, delimiter=',')
    #     line_count = 0
    #     for row in csv_reader:
    #         if line_count == 0:
    #             pass
    #         else:
    #             subdir = row[0]
                    # xmin = int(float(row[1]))
                    # ymin = int(float(row[2]))
                    # xmax = int(float(row[3]))
                    # ymax = int(float(row[4]))
    #             if prevImgTrain != subdir:
    #                 currentImgNum += 1
    #                 prevImgTrain = subdir
    #                 origImgFile = "E:/WIDER FACE/" + subdir
    #                 outputImgFile = OUTPUT_TRAIN_IMG + str(currentImgNum).zfill(8) + ".png"
    #                 copyfile(origImgFile, outputImgFile)
    #                 rowdata = [str(currentImgNum).zfill(8) + ".png", str(xmin), str(xmax), str(ymin), str(ymax), str(1)]
    #                 print(rowdata)
    #                 csvData_train.append(rowdata)
    #             else:
    #                 rowdata = [str(currentImgNum).zfill(8) + ".png", str(xmin), str(xmax), str(ymin), str(ymax), str(1)]
    #                 print(rowdata)
    #                 csvData_train.append(rowdata)
    #         line_count += 1
    with open('E:\\WIDER FACE\\mergeddatasets_val.csv', 'w') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerows(csvData_train)
    csvFile.close()

def checkCSVFileError():
        with open('E:\\WIDER FACE\\trainset.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0
            for row in csv_reader:
                if line_count == 0:
                    pass
                else:
                    subdir = row[0]
                    xmin = int(row[1])
                    xmax = int(row[2])
                    ymin = int(row[3])
                    ymax = int(row[4])
                    hasHead = int(row[5])
                    if (xmin >= 0 and xmin < 640 and
                            xmax >= 0 and xmax < 640 and
                                ymin >= 0 and ymin < 480 and
                                    ymax >= 0 and ymax < 480 and
                                        hasHead == 1): continue
                    else:
                        print("Something wrong with the csv file")
                        break
            line_count += 1
def removeExtraPNG():
    with open('E:\\finalheadboundingbox\\HeadBoundingBoxDataFinal\\head_bounding_box_test_out.csv') as inf, open('E:\\finalheadboundingbox\\HeadBoundingBoxDataFinal\\head_bounding_box_test_out_final.csv', 'w') as outf:
        reader = csv.reader(inf)
        writer = csv.writer(outf)
        line_count = 0

        for line in reader:
            # [['imgnum', 'xmin', 'ymin', 'xmax', 'ymax', 'hasHead']]
            if line_count == 0:
                writer.writerow([line[0], line[1], line[3], line[2], line[4], line[5]])
            else:
                imgFilename = line[0]
                xmin = int(float(line[1]))
                xmax = int(float(line[2]))
                ymin = int(float(line[3]))
                ymax = int(float(line[4]))
                hasHead = int(line[5])
                correctImgFileName = imgFilename[0:8] + ".png"
                if (hasHead == 1):
                    writer.writerow([correctImgFileName, str(xmin), str(xmax), str(ymin), str(ymax), str(hasHead)])
            line_count += 1
if __name__ == "__main__":
    removeExtraPNG()
