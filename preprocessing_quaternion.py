import numpy as np
import math
import os
import re
import random
from PIL import Image
from matplotlib import cm
from shutil import copyfile
import csv
import cv2
import json

GAZEDATASET_ROOT = "D:\\GazeDatasets\\"
OUTPUT_DIR = "D:\\GazeDatasets30K\\"
FDDBDATASET_ROOT = "E:\\FDDBDatasetNew\\"
FDDBDATASET_OUTPUT = "E:\\FDDBDatasetNew\\"
WIDERFACEDATASET_ROOT = "E:\\jasper\\WiderFace_Parser\\WIDER_test\\images\\"
MERGED_DATASET_OUTPUT = "E:\\FinalDatasetOutput\\"
HEAD_BOUNDINGBOX_ROOT = "E:\\finalheadboundingbox\\HeadBoundingBoxData\\test_imgs\\"
HEAD_BOUNDINGBOX_OUTPUT = "D:\\GazeDatasets30K\\test\\"
OBJECT_INTEREST_DATAROOT = os.environ['HOME'] + "/Documents/MSEEThesis/gazeoverobject/datasets/ObjectOfInterestDatasetSept25/"

def getFMatrix(roll, pitch, yaw):
    SR = math.sin(math.radians(roll))
    SP = math.sin(math.radians(pitch))
    SY = math.sin(math.radians(yaw))
    CR = math.cos(math.radians(roll))
    CP = math.cos(math.radians(pitch))
    CY = math.cos(math.radians(yaw))
    M = np.zeros((3,3))
    M[0][0]=CP*CY
    M[0][1]=CP*SY
    M[0][2]=SP
    M[1][0]=SR*SP*CY-CR*SY
    M[1][1]=SR*SP*SY+CR*CY
    M[1][2]=-SR*CP
    M[2][0]=-(CR*SP*CY+SR*SY)
    M[2][1]=(CY*SR)-(CR*SP*SY)
    M[2][2]=CR*CP
    return M

### STEP 1
def preprocessquats():
    for root, dirs, files in os.walk(GAZEDATASET_ROOT):
        for file in files:
            if file.endswith(".txt"):
                fulltextfilename = os.path.join(root, file)
                # print(fulltextfilename)
                myfile = open(fulltextfilename, "rt") # open lorem.txt for reading text
                contents = myfile.read()         # read the entire file into a string
                myfile.close()                   # close the file
                # print(contents)                  # print contents
                text = contents
                pitch = re.search('P=(.+?) Y=', text).group(1)
                yaw = re.search('Y=(.+?) R=', text).group(1)
                roll = re.search('R=(.+?);X=', text).group(1)
                xpos = re.search('X=(.+?) Y=', text).group(1)
                ypostxt = text[text.rfind('Y'):len(text)]
                ypos = re.search('Y=(.+?) Z=', ypostxt).group(1)
                zpos = text.split("Z=",1)[1]
                M = getFMatrix(float(roll), float(pitch), -float(yaw))
                print(fulltextfilename)
                finalTxtStr = str(M[0][0]) + " " + str(M[0][1]) + " " + str(M[0][2]) + " \n" + str(M[1][0]) + " " + str(M[1][1]) + " " + str(M[1][2]) + " \n" + str(M[2][0]) + " " + str(M[2][1]) + " " + str(M[2][2]) + " \n\n" + str(xpos) + " " + str(ypos) + " " + str(zpos)
                fullfilepath = fulltextfilename[0:fulltextfilename.rfind('\\')] + "\\"
                filenumname = fulltextfilename[fulltextfilename.rindex('\\')+1:][:-4]
                newfilename = filenumname + "final"
                fullnewfilename = fullfilepath + newfilename + ".txt"
                text_file = open(fullnewfilename, "w")
                text_file.write(finalTxtStr)
                text_file.close()

def preprocessquats_singleFile(filename):
    fulltextfilename = filename
    # print(fulltextfilename)
    myfile = open(fulltextfilename, "rt") # open lorem.txt for reading text
    contents = myfile.read()         # read the entire file into a string
    myfile.close()                   # close the file
    text = contents
    pitch = re.search('P=(.+?) Y=', text).group(1)
    yaw = re.search('Y=(.+?) R=', text).group(1)
    roll = re.search('R=(.+?);X=', text).group(1)
    xpos = re.search('X=(.+?) Y=', text).group(1)
    ypostxt = text[text.rfind('Y'):len(text)]
    ypos = re.search('Y=(.+?) Z=', ypostxt).group(1)
    zpos = text.split("Z=",1)[1]
    M = getFMatrix(float(roll), float(pitch), -float(yaw))
    print(fulltextfilename)
    finalTxtStr = str(M[0][0]) + " " + str(M[0][1]) + " " + str(M[0][2]) + " \n" + str(M[1][0]) + " " + str(M[1][1]) + " " + str(M[1][2]) + " \n" + str(M[2][0]) + " " + str(M[2][1]) + " " + str(M[2][2]) + " \n\n" + str(xpos) + " " + str(ypos) + " " + str(zpos)
    fullfilepath = fulltextfilename[0:fulltextfilename.rfind('\\')] + "\\"
    filenumname = fulltextfilename[fulltextfilename.rindex('\\')+1:][:-4]
    newfilename = filenumname + "final"
    fullnewfilename = fullfilepath + newfilename + ".txt"
    text_file = open(fullnewfilename, "w")
    text_file.write(finalTxtStr)
    text_file.close()

### Capture cone segmentation
# point = in (x,y) the point to check
# center = in (x,y) the circle center
# r = the circle radius
def isInsideCircle(point, center, r):
    return ((point[0] - center[0])**2 + (point[1] - center[1])**2) <= r**2
def areClockwise(v1, v2):
    return -v1[0]*v2[1] + v1[1]*v2[0] > 0
def isWithinRadius(v, radiusSquared):
    return v[0]*v[0] + v[1]*v[1] <= radiusSquared
def convertToAcute(theta):
    # print("theta: ", theta)
    if  (theta < math.pi and theta >= 0.5*math.pi):
        return theta + 2*math.pi
    if  (theta < 1.5*math.pi and theta >= math.pi):
        return theta
    if  (theta >= 1.5*math.pi):
        return theta + 2*math.pi
    if (theta < 0.5*math.pi):
        return theta + 2*math.pi

###
# point = point to check
# center = the head loc
# sectorStart = v1
# sectorEnd = v2
# radiusSquared = the radius from the center to the end (this is because the calculated region is assummed to be a circular section rather than a cone)
# circle_center - from the minimum bounding circle
def isInsideSector(point, center, sectorStart, sectorEnd, radiusSquared, circle_center, circle_radius):
    relPoint = (point[0] - center[0], point[1] - center[1])
    if isInsideCircle(point, circle_center, circle_radius): return True
    if (not areClockwise(sectorStart, relPoint)) and areClockwise(sectorEnd, relPoint) and isWithinRadius(relPoint, radiusSquared): return True
    return False
    # return (isInsideCircle(point, circle_center, circle_radius)) or ((not areClockwise(sectorStart, relPoint)) and areClockwise(sectorEnd, relPoint) and isWithinRadius(relPoint, radiusSquared))
def checkIfPointInsideCone():
    for root, dirs, files in os.walk(OBJECT_INTEREST_DATAROOT):
        for file in files:
            if file.endswith('rgb.png'):
                fullimgfilename = os.path.join(root, file)
                # fullimgfilename = os.environ['HOME'] + "/Documents/MSEEThesis/gazeoverobject/datasets/ObjectOfInterestDatasetSept25/Human4/cam1/3-4-3-rgb.png"
                print(fullimgfilename)
                fullfilepath = fullimgfilename[0:fullimgfilename.rfind('/')] + "/"
                filenumname = fullimgfilename[fullimgfilename.rindex('/')+1:][:-7]
                txtfilename = fullfilepath + filenumname + ".txt"
                headlocfilename = fullfilepath + filenumname + "headlocbinmsk.png"
                headwholefilename = fullfilepath + filenumname + "headwholebinmsk.png"
                itemlocfilename = fullfilepath + filenumname + "itemlocbinmsk.png"
                itemwholefilename = fullfilepath + filenumname + "itemwholebinmsk.png"
                jsonfile = fullfilepath + filenumname + "final.json"
                conesegmimgfile = fullfilepath + filenumname + "conesegm.png"
                circlesegmimgfile = fullfilepath + filenumname + "circlesegm.png"

                with open(jsonfile) as f:
                    jsondata = json.load(f)

                width = 640
                height = 480
                circle_centerX = int(jsondata["item_circleX"])
                circle_centerY = int(jsondata["item_circleY"])
                circle_center = (circle_centerX, circle_centerY)
                circle_radius = int(jsondata["item_circleRad"])
                hx = int(jsondata["headLocX"])
                hy = int(jsondata["headLocY"])
                center = (hx, hy)

                circlesegmimg = Image.new('RGB', (width, height))
                circlesegmPixels = circlesegmimg.load()
                newimg = Image.new('RGB', (width, height))
                pixelsNew = newimg.load()
                for x in range(width):
                    for y in range(height):
                        point = (x, y)
                        hx = int(jsondata["headLocX"])
                        hy = int(jsondata["headLocY"])
                        center = (hx, hy)
                        v = math.sqrt((circle_center[0]-center[0])**2 + (circle_center[1]-center[1])**2)
                        r = v
                        theta = math.atan((circle_center[1]-center[1])/(circle_center[0]-center[0]))
                        # print(circle_center[0]-center[0])
                        alpha = math.atan(circle_radius/v) ### Answer in radians
                        # print("theta: ", theta)
                        # print("alpha: ", alpha)
                        v1 = circle_radius/math.sin(alpha)
                        v2 = v1
                        if circle_center[0]-center[0] >= 0:
                            v1x = v1 * math.cos((theta + alpha))
                            v1y = v1 * math.sin((theta + alpha))
                            v2x = v2 * math.cos((theta - alpha))
                            v2y = v2 * math.sin((theta - alpha))
                            sectorStart = (v1x, v1y)
                            sectorEnd = (v2x, v2y)
                            radiusSquared = r**2
                            isInsideConeSector = isInsideSector(point, center, sectorEnd, sectorStart, radiusSquared, circle_center, circle_radius)
                        else:
                            v1x = v1 * math.cos((theta + alpha))
                            v1y = v1 * math.sin((theta + alpha))
                            v2x = v2 * math.cos((theta - alpha))
                            v2y = v2 * math.sin((theta - alpha))
                            sectorStart = (v1x, v1y)
                            sectorEnd = (v2x, v2y)
                            radiusSquared = r**2
                            isInsideConeSector = isInsideSector(point, center, sectorStart, sectorEnd, radiusSquared, circle_center, circle_radius)
                        # print("v1x: ", v1x)
                        # print("v1y: ", v1y)
                        # print("v2x: ", v2x)
                        # print("v2y: ", v2y)
                        # print("isInside: ", isInsideConeSector)
                        if isInsideConeSector: pixelsNew[x, y] = (255, 255, 255)
                        else: pixelsNew[x, y] = (0, 0, 0)
                        if isInsideCircle(point, circle_center, circle_radius): circlesegmPixels[x, y] = (255, 255, 255)
                        else: circlesegmPixels[x, y] = (0, 0, 0)
                print(conesegmimgfile)
                print(circlesegmimgfile)
                newimg.save(conesegmimgfile)
                newimg.close()
                circlesegmimg.save(circlesegmimgfile)
                circlesegmimg.close()
def cropImageAtCenter(imgdir, new_width, new_height):
    # preprocessrgbs()
    # im = Image.open("C:\\Users\\marky\\Documents\\GazeEstimationProjects\\FSA-Net\\GazeDatasets\\MaleBlack1\\cam1\\1.png")
    im = Image.open(imgdir)
    width, height = im.size   # Get dimensions

    # new_width = 640
    # new_height = 480
    left = (width - new_width)/2
    top = (height - new_height)/2
    right = (width + new_width)/2
    bottom = (height + new_height)/2

    # Crop the center of the image
    im = im.crop((left, top, right, bottom))
    return im
    # im.save("C:\\Users\\marky\\Documents\\GazeEstimationProjects\\FSA-Net\\GazeDatasets\\MaleBlack1\\cam1\\1-out.png")

### STEP 2 (IF NOT OPTIMAL RESOLUTION, CALL THIS)
def preprocessrgbs():
    for root, dirs, files in os.walk(GAZEDATASET_ROOT):
        for file in files:
            if file.endswith(".png"):
                fullimgfilename = os.path.join(root, file)
                fullfilepath = fullimgfilename[0:fullimgfilename.rfind('\\')] + "\\"
                filenumname = fullimgfilename[fullimgfilename.rindex('\\')+1:][:-4]
                newfilename = filenumname + "final"
                fullnewfilename = fullfilepath + newfilename + ".png"

                im = cropImageAtCenter(fullimgfilename, 640, 480)
                im.save(fullnewfilename)
                print(fullnewfilename)

# Pass in an ndarray img, (0, 0) is the top left index of the img
def getBBoxCoordinates(imgA):
    B = np.argwhere(imgA)
    (ymin, xmin), (ymax, xmax) = B.min(0), B.max(0)
    return xmin, ymin, xmax, ymax

def testBBoxCoords():
    im = Image.open("D:\\GazeDatasets\\Human1\\cam1\\106-binmsk.png")
    imInNDArr = np.array(im)
    imInNDArr = imInNDArr[:,:,0]
    imInNDArr[imInNDArr > 0] = 1
    xmin, ymin, xmax, ymax = getBBoxCoordinates(imInNDArr)
    print("xmin:" + str(xmin) + " ymin:" + str(ymin) + " xmax:" + str(xmax) + " ymax:" + str(ymax))

def refixBBoxAnnots():
    for root, dirs, files in os.walk(GAZEDATASET_ROOT):
        for file in files:
            if file.endswith('binmsk.png'):
                fullimgfilename = os.path.join(root, file)
                im = Image.open(fullimgfilename)

def fixSaveAndReplaceSegmImg(imgFileName):
    # im = Image.open("D:\\GazeDatasets\\Human1\\cam1\\1-binmsk.png")
    im = Image.open(imgFileName)
    im = im.convert('RGB')
    width, height = im.size
    pixels = im.load()

    newimg = Image.new('RGB', im.size)
    pixelsNew = newimg.load()

    for x in range(width):
        for y in range(height):
            color = pixels[x, y]
            r = color[0]
            g = color[1]
            b = color[2]
            Y = 0.2126*r + 0.7152*g + 0.0722*b
            isWhite = False if (Y < 128) else True
            if isWhite: pixelsNew[x, y] = (255, 255, 255)
            else: pixelsNew[x, y] = (0, 0, 0)
    im.close()
    newimg.save(imgFileName)
    newimg.close()

def fixBBoxImages():
    for root, dirs, files in os.walk(GAZEDATASET_ROOT):
        for file in files:
            if file.endswith('binmsk.png'):
                fullimgfilename = os.path.join(root, file)
                im2 = Image.open(fullimgfilename)
                imInNDArr2 = np.array(im2)
                imInNDArr2 = imInNDArr2[:,:,0]
                imInNDArr2[imInNDArr2 > 0] = 1
                newxmin, newymin, newxmax, newymax = getBBoxCoordinates(imInNDArr2)
                finalHeadBBoxStr = str(newxmin) + " " + str(newymin) + " " + str(newxmax) + " " + str(newymax)
                fullfilepath = fullimgfilename[0:fullimgfilename.rfind('\\')] + "\\"
                filenumname = fullimgfilename[fullimgfilename.rindex('\\')+1:][:-11]
                bboxfullfilename = fullfilepath + filenumname + "-bbox.txt"
                print("bboxfullfilename:" + bboxfullfilename)
                bbox_text_file = open(bboxfullfilename, "w")
                bbox_text_file.write(finalHeadBBoxStr)
                bbox_text_file.close()

### STEP 3
def createBoundingBox():
    # csvData = [['filename', 'xmin', 'ymin', 'xmax', 'ymax'], ['GazeDatasets\\Human1\\cam1', '22', '22', '22', '22']]
    csvData = [['filename', 'xmin', 'ymin', 'xmax', 'ymax']]
    for root, dirs, files in os.walk(GAZEDATASET_ROOT):
        for file in files:
            if file.endswith('binmsk.png'):
                fullimgfilename = os.path.join(root, file)
                # print(fullimgfilename)
                im = Image.open(fullimgfilename)
                # im = cropImageAtCenter(fullimgfilename, 640, 480)
                imInNDArr = np.array(im)
                # print("amax:", np.amax(imInNDArr))
                # imInNDArr[imInNDArr > 0] = 255 #
                # image = Image.fromarray(imInNDArr)
                # image = image.convert('1')
                # image.show()
                imInNDArr = imInNDArr[:,:,0]
                imInNDArr[imInNDArr > 0] = 1
                xmin, ymin, xmax, ymax = getBBoxCoordinates(imInNDArr)
                substr = "GazeDatasets\\"
                fileimgn = substr + fullimgfilename.split(substr, 1)[1]
                fileimgn = fileimgn[:-11] + ".png"
                rowdata = [fileimgn, str(xmin), str(ymin), str(xmax), str(ymax)]
                print(rowdata)
                csvData.append(rowdata)
    with open(OUTPUT_DIR + 'head_bounding_box.csv', 'w', newline='') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerows(csvData)
    csvFile.close()

def getXYHeadLocation():
    for root, dirs, files in os.walk(GAZEDATASET_ROOT):
        for file in files:
            if file.endswith('binmskheadpos.png'):
                fullimgfilename = os.path.join(root, file)
                im = Image.open(fullimgfilename)
                imInNDArr = np.array(im)
                imInNDArr = imInNDArr[:,:,0]
                imInNDArr[imInNDArr > 0] = 1
                xmin, ymin, xmax, ymax = getBBoxCoordinates(imInNDArr)
                headLocX = (xmin + xmax) / 2
                headLocY = (ymin + ymax) / 2

                fullfilepath = fullimgfilename[0:fullimgfilename.rfind('\\')] + "\\"
                filenumname = fullimgfilename[fullimgfilename.rindex('\\')+1:][:-18]
                txtfilename = fullfilepath + filenumname + ".txt"
                # print("newfilename: ", txtfilename)

                myfile = open(txtfilename, "rt") # open lorem.txt for reading text
                contents = myfile.read()         # read the entire file into a string
                myfile.close()                   # close the file
                # print(contents)                  # print contents
                text = contents
                pitch = re.search('P=(.+?) Y=', text).group(1)
                yaw = re.search('Y=(.+?) R=', text).group(1)
                roll = re.search('R=(.+?);X=', text).group(1)
                zpos = text.split("X=",1)[1]
                xpos = headLocX
                ypos = headLocY
                M = getFMatrix(float(roll), float(pitch), -float(yaw))
                finalTxtStr = str(M[0][0]) + " " + str(M[0][1]) + " " + str(M[0][2]) + " \n" + str(M[1][0]) + " " + str(M[1][1]) + " " + str(M[1][2]) + " \n" + str(M[2][0]) + " " + str(M[2][1]) + " " + str(M[2][2]) + " \n\n" + str(xpos) + " " + str(ypos) + " " + str(zpos)
                fulltxtfinal = fullfilepath + filenumname + "final.txt"

                ## Get full head bounding box
                fullimgbboxfilename = fullfilepath + filenumname + "-binmsk.png"
                im2 = Image.open(fullimgbboxfilename)
                imInNDArr2 = np.array(im2)
                imInNDArr2 = imInNDArr2[:,:,0]
                imInNDArr2[imInNDArr2 > 0] = 1
                newxmin, newymin, newxmax, newymax = getBBoxCoordinates(imInNDArr2)
                finalHeadBBoxStr = str(newxmin) + " " + str(newymin) + " " + str(newxmax) + " " + str(newymax)
                print("xmin:" + str(newxmin) + " ymin:" + str(newymin) + " xmax:" + str(newxmax) + " ymax:" + str(newymax))
                bboxfullfilename = fullfilepath + filenumname + "-bbox.txt"

                print(fulltxtfinal)
                text_file = open(fulltxtfinal, "w")
                text_file.write(finalTxtStr)
                text_file.close()
                print(finalHeadBBoxStr)
                bbox_text_file = open(bboxfullfilename, "w")
                bbox_text_file.write(finalHeadBBoxStr)
                bbox_text_file.close()

def refixSegmFiles():
    for root, dirs, files in os.walk(OBJECT_INTEREST_DATAROOT):
        for file in files:
            if file.endswith('rgb.png'):
                fullimgfilename = os.path.join(root, file)
                print(fullimgfilename)
                fullfilepath = fullimgfilename[0:fullimgfilename.rfind('\\')] + "\\"
                filenumname = fullimgfilename[fullimgfilename.rindex('\\')+1:][:-7]
                txtfilename = fullfilepath + filenumname + ".txt"
                headlocfilename = fullfilepath + filenumname + "headlocbinmsk.png"
                headwholefilename = fullfilepath + filenumname + "headwholebinmsk.png"
                itemlocfilename = fullfilepath + filenumname + "itemlocbinmsk.png"
                itemwholefilename = fullfilepath + filenumname + "itemwholebinmsk.png"
                fixSaveAndReplaceSegmImg(headlocfilename)
                fixSaveAndReplaceSegmImg(headwholefilename)
                fixSaveAndReplaceSegmImg(itemlocfilename)
                fixSaveAndReplaceSegmImg(itemwholefilename)

def createSegmLocAndFinalInfoTxtFile():
    for root, dirs, files in os.walk(OBJECT_INTEREST_DATAROOT):
        for file in files:
            if file.endswith('rgb.png'):
                fullimgfilename = os.path.join(root, file)
                print(fullimgfilename)
                fullfilepath = fullimgfilename[0:fullimgfilename.rfind('\\')] + "\\"
                filenumname = fullimgfilename[fullimgfilename.rindex('\\')+1:][:-7]
                headlocfilename = fullfilepath + filenumname + "headlocbinmsk.png"
                headwholefilename = fullfilepath + filenumname + "headwholebinmsk.png"
                itemlocfilename = fullfilepath + filenumname + "itemlocbinmsk.png"
                itemwholefilename = fullfilepath + filenumname + "itemwholebinmsk.png"
                txtfilename = fullfilepath + filenumname + "info.txt"
                myfile = open(txtfilename, "rt")
                contents = myfile.read()
                myfile.close()
                text = contents
                print("contents: " + contents)
                pitch = re.search('P=(.+?) Y=', text).group(1)
                yaw = re.search('Y=(.+?) R=', text).group(1)
                roll = re.search('R=(.+?);Z=', text).group(1)
                zpos = re.search('Z=(.+?) IZ=', text).group(1)
                item_zpos = re.search('IZ=(.+?) Ino=', text).group(1)
                item_no = text.split("Ino=",1)[1]

                im_headloc = Image.open(headlocfilename)
                imInNDArr_headloc = np.array(im_headloc)
                imInNDArr_headloc = imInNDArr_headloc[:,:,0]
                imInNDArr_headloc[imInNDArr_headloc > 0] = 1
                xmin_headloc, ymin_headloc, xmax_headloc, ymax_headloc = getBBoxCoordinates(imInNDArr_headloc)
                headLocX = (xmin_headloc + xmax_headloc) / 2
                headLocY = (ymin_headloc + ymax_headloc) / 2
                im_headloc.close()

                im_itemloc = Image.open(itemlocfilename)
                imInNDArr_itemloc = np.array(im_itemloc)
                imInNDArr_itemloc = imInNDArr_itemloc[:,:,0]
                imInNDArr_itemloc[imInNDArr_itemloc > 0] = 1
                xmin_itemloc, ymin_itemloc, xmax_itemloc, ymax_itemloc = getBBoxCoordinates(imInNDArr_itemloc)
                itemLocX = (xmin_itemloc + xmax_itemloc) / 2
                itemLocY = (ymin_itemloc + ymax_itemloc) / 2
                im_itemloc.close()

                im_headwhole = Image.open(headwholefilename)
                imInNDArr_headwhole = np.array(im_headwhole)
                imInNDArr_headwhole = imInNDArr_headwhole[:,:,0]
                imInNDArr_headwhole[imInNDArr_headwhole > 0] = 1
                xmin_headwhole, ymin_headwhole, xmax_headwhole, ymax_headwhole = getBBoxCoordinates(imInNDArr_headwhole)
                im_headwhole.close()

                im_itemwhole = Image.open(itemwholefilename)
                imInNDArr_itemwhole = np.array(im_itemwhole)
                imInNDArr_itemwhole = imInNDArr_itemwhole[:,:,0]
                imInNDArr_itemwhole[imInNDArr_itemwhole > 0] = 1
                xmin_itemwhole, ymin_itemwhole, xmax_itemwhole, ymax_itemwhole = getBBoxCoordinates(imInNDArr_itemwhole)
                im_itemwhole.close()

                fulltxtfinal = fullfilepath + filenumname + "final.json"
                data_dict = {}
                data_dict['pitch'] = float(pitch)
                data_dict['yaw'] = float(pitch)
                data_dict['roll'] = float(roll)
                data_dict['headLocX'] = float(headLocX)
                data_dict['headLocY'] = float(headLocY)
                data_dict['headLocZ'] = float(zpos)
                data_dict['itemLocX'] = float(itemLocX)
                data_dict['itemLocY'] = float(itemLocY)
                data_dict['itemLocZ'] = float(item_zpos)
                data_dict['itemNum'] = int(item_no)
                data_dict['xmin_headbb'] = float(xmin_headwhole)
                data_dict['ymin_headbb'] = float(ymin_headwhole)
                data_dict['xmax_headbb'] = float(xmax_headwhole)
                data_dict['ymax_headbb'] = float(ymax_headwhole)
                data_dict['xmin_itembb'] = float(xmin_itemwhole)
                data_dict['ymin_itembb'] = float(ymin_itemwhole)
                data_dict['xmax_itembb'] = float(xmax_itemwhole)
                data_dict['ymax_itembb'] = float(ymax_itemwhole)
                with open(fulltxtfinal, 'w') as fp:
                    json.dump(data_dict, fp)

def getBoundingBoxData(binSegmImgFileName):
    im = Image.open(binSegmImgFileName)
    imInNDArr = np.array(im)
    imInNDArr = imInNDArr[:,:,0]
    imInNDArr[imInNDArr > 0] = 1
    xmin, ymin, xmax, ymax = getBBoxCoordinates(imInNDArr)
    return xmin, ymin, xmax, ymax

def saveSingleBoundingBoxData(binSegmImgFileName, outputFileName):
    xmin, ymin, xmax, ymax = getBoundingBoxData(binSegmImgFileName)
    text_file = open(outputFileName, "w")
    finalTxtStr = str(xmin) + " " + str(ymin) + " " + str(xmax) + " " + str(ymax)
    text_file.write(finalTxtStr)
    text_file.close()

def checkIfValidSetup(binSegmFileName, txtDataFileName):
    return os.path.isfile(binSegmFileName) and os.path.isfile(txtDataFileName)

def checkIfValidSetup(imgFileName, binSegmFileName, txtDataFileName):
    return os.path.isfile(binSegmFileName) and os.path.isfile(txtDataFileName)

def preprocessDataset():
    imageNum = 0
    csvData = [['imgnum', 'xmin', 'ymin', 'xmax', 'ymax', 'hasHead']]
    for root, dirs, files in os.walk(GAZEDATASET_ROOT):
        for file in files:
            if file.endswith('png'):
                fullimgfilename = os.path.join(root, file)
                # print(fullimgfilename)
                if file.endswith('binmsk.png'):
                    # Bin Segm
                    pass
                else:
                    # RGB img
                    if not os.path.exists(OUTPUT_DIR + "images\\"):
                        os.makedirs(OUTPUT_DIR + "images\\")
                    dstimg = OUTPUT_DIR + "images\\" + str(imageNum).zfill(8) + ".png"
                    fullStrFileNumName = fullimgfilename.split("skbx")[0]
                    binSegmFileName = fullStrFileNumName + "-binmsk.png"
                    txtDataFileName = fullStrFileNumName + ".txt"
                    if checkIfValidSetup(binSegmFileName, txtDataFileName):
                        headInPic = 1
                        copyfile(fullimgfilename, dstimg)
                        print(dstimg)
                        if "cam5" in fullimgfilename:
                            print("Came5Detected")
                            headInPic = 0
                            xmin = -1
                            ymin = -1
                            xmax = -1
                            ymax = -1
                        else:
                            xmin, ymin, xmax, ymax = getBoundingBoxData(binSegmFileName)
                        rowdata = [str(imageNum).zfill(8), str(xmin), str(ymin), str(xmax), str(ymax), str(headInPic)]
                        csvData.append(rowdata)
                        imageNum = imageNum + 1
    with open(OUTPUT_DIR + 'head_bounding_box.csv', 'w', newline='') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerows(csvData)
    csvFile.close()

# Create images and csv files for FDDB Dataset
def preprocessFDDBDataset():
    imageNum = -1
    current_csv_num = 1
    csvData = [['imgnum', 'xmin', 'ymin', 'xmax', 'ymax', 'hasHead']]
    prevImg = ""

    while current_csv_num <= 10:
        with open(FDDBDATASET_ROOT + 'FDDB-fold-' + str(current_csv_num).zfill(2) + '-rectList.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0
            for row in csv_reader:
                if line_count == 0:
                    print(f'Column names are {", ".join(row)}')
                else:
                    subdir = row[0]
                    if prevImg != subdir:
                        subdir = subdir.replace('/', "\\")
                        fullimgfilename = FDDBDATASET_ROOT + subdir
                        xmin = float(row[1])
                        ymin = float(row[2])
                        xmax = float(row[3])
                        ymax = float(row[4])
                        imageNum += 1
                        dstimg = FDDBDATASET_OUTPUT + "images\\" + str(imageNum).zfill(8) + ".png"
                        copyfile(fullimgfilename, dstimg)
                        print(dstimg)
                        rowdata = [str(imageNum).zfill(8), str(xmin), str(ymin), str(xmax), str(ymax), str(1)]
                        csvData.append(rowdata)
                        prevImg = row[0]
                    else:
                        print("Duplicate BBox")
                        xmin = float(row[1])
                        ymin = float(row[2])
                        xmax = float(row[3])
                        ymax = float(row[4])
                        rowdata = [str(imageNum).zfill(8), str(xmin), str(ymin), str(xmax), str(ymax), str(1)]
                        csvData.append(rowdata)
                line_count += 1
        current_csv_num = current_csv_num + 1
    with open(FDDBDATASET_OUTPUT + 'fddb_head_bounding_box.csv', 'w', newline='') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerows(csvData)
    csvFile.close()

    # imageNum = 0
    # csvData = [['imgnum', 'xmin', 'ymin', 'xmax', 'ymax', 'hasHead']]

    #         if file.endswith('png'):
    #             fullimgfilename = os.path.join(root, file)

def sampleFromImagesFromGazeDataset():
    dir_list = next(os.walk(GAZEDATASET_ROOT))[1]
    alreadyAddedFile = []
    imageNum = 0
    csvData = [['filename', 'imgnum', 'xmin', 'xmax', 'ymin', 'ymax', 'hasHead']]

    for folderName in dir_list:
        for camNum in range(1,9):
            for i in range(80):
                while True:
                    randomFrame = random.randint(1, 149)
                    randomSkyBoxNum = random.randint(1, 10)
                    # Sample random image 'subdirname'
                    subdirname = folderName + "\\cam" + str(camNum) + "\\" + str(randomFrame) + "skbx" + str(randomSkyBoxNum) + ".png"
                    fullStrFileNumName = subdirname.split("skbx")[0]
                    binSegmFileName = GAZEDATASET_ROOT + fullStrFileNumName + "-binmsk.png"
                    txtDataFileName = GAZEDATASET_ROOT + fullStrFileNumName + ".txt"
                    if not os.path.isfile(GAZEDATASET_ROOT + subdirname): continue
                    if not os.path.isfile(binSegmFileName): continue
                    if not os.path.isfile(txtDataFileName): continue
                    if subdirname in alreadyAddedFile: continue

                    dstimg = OUTPUT_DIR + "images\\" + str(imageNum).zfill(8) + ".png"
                    xmin, ymin, xmax, ymax = getBoundingBoxData(binSegmFileName)
                    rowdata = [subdirname, str(imageNum).zfill(8), str(xmin), str(xmax), str(ymin), str(ymax), str(1)]
                    csvData.append(rowdata)

                    copyfile(GAZEDATASET_ROOT + subdirname, dstimg)
                    alreadyAddedFile.append(subdirname)
                    imageNum += 1
                    print(subdirname)
                    break
            alreadyAddedFile = []
    with open(OUTPUT_DIR + 'head_bounding_box.csv', 'w', newline='') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerows(csvData)
    csvFile.close()

def preprocessWiderFaceDataset():
    prevImg = ""
    with open(WIDERFACEDATASET_ROOT + 'face_train_new.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                pass
            else:
                subdir = row[0]
                if prevImg != subdir:
                    subdir = subdir.replace('/', "\\")
                    fullimgfilename = WIDERFACEDATASET_ROOT + subdir
                    xmin = float(row[1])
                    ymin = float(row[2])
                    xmax = float(row[3])
                    ymax = float(row[4])
                    dstimg = FDDBDATASET_OUTPUT + "images\\" + str(imageNum).zfill(8) + ".png"
                    copyfile(fullimgfilename, dstimg)
            line_count += 1

def resizeImgAndBoundingBox(imgFileName, outputXSize, outputYSize, bboxXmin, bboxYmin, bboxXmax, bboxYmax):
    # imageToPredict = cv2.imread("img.jpg", 3)
    imageToPredict = cv2.imread(imgFileName, 3)
    y_ = imageToPredict.shape[0]
    x_ = imageToPredict.shape[1]
    targetSizeX = outputXSize
    targetSizeY = outputYSize
    x_scale = targetSizeX / x_
    y_scale = targetSizeY / y_
    img = cv2.resize(imageToPredict, (targetSizeX, targetSizeY));
    img = np.array(img);

    # original frame as named values
    (origLeft, origTop, origRight, origBottom) = (bboxXmin, bboxYmin, bboxXmax, bboxYmax)

    x = int(np.round(origLeft * x_scale))
    y = int(np.round(origTop * y_scale))
    xmax = int(np.round(origRight * x_scale))
    ymax = int(np.round(origBottom * y_scale))
    return img, x, y, xmax, ymax

# Clean any CSV File, remove all empty lines
def removeAllNewLinesCSV():
    with open('D:\\GazeDatasets30K\\test.csv') as infile, open('D:\\GazeDatasets30K\\test_nolines.csv', 'w') as outfile:
        for line in infile:
            if not line.strip(): continue  # skip the empty line
            outfile.write(line)  # non-empty line. Write it to output

def cleanFDDBDataset():
    prevImg = ""
    csvData = [['imgnum', 'xmin', 'ymin', 'xmax', 'ymax', 'hasHead']]
    with open(FDDBDATASET_ROOT + 'fddb_head_bounding_box_final.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                pass
            else:
                print(row[0])
                subdir = row[0]
                subdir = subdir.replace('/', "\\")
                fullimgfilename = FDDBDATASET_ROOT + "images\\" + subdir + ".png"
                xmin = float(row[1])
                ymin = float(row[2])
                xmax = float(row[3])
                ymax = float(row[4])
                dstimg = FDDBDATASET_OUTPUT + "images2\\" + subdir + ".png"
                img, xmin, ymin, xmax, ymax = resizeImgAndBoundingBox(fullimgfilename, 640, 480, xmin, ymin, xmax, ymax)
                # copyfile(fullimgfilename, dstimg)
                cv2.imwrite(dstimg, img)
                rowdata = [subdir, str(xmin), str(ymin), str(xmax), str(ymax), str(1)]
                print(rowdata)
                csvData.append(rowdata)
            line_count += 1
    with open(FDDBDATASET_OUTPUT + 'head_bounding_box_cleaned_resized.csv', 'w', newline='') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerows(csvData)
    csvFile.close()

def openImageAndSaveWithoutAlpha(pillowImg, ):
    try:
        im = Image.open(original_file_path)
        im.thumbnail(size)
        im.save(original_file_path)
    except IOError:
        print("cannot create thumbnail for", original_file_path)

def convertImgToNongrayscale(imgfilename, imgtarget):
    img = Image.open(imgfilename)
    rgbimg = Image.new("RGB", img.size)
    rgbimg.paste(img)
    rgbimg.save(imgtarget)

def renameImagesInFolder():
    currentNum = 141488     # Change this to the last number plus one of the last image number in the training set
    with open('E:\\finalheadboundingbox\\HeadBoundingBoxDataFinal\\head_bounding_box_test_final.csv') as inf, open('E:\\finalheadboundingbox\\HeadBoundingBoxDataFinal\\head_bounding_box_test_final_out.csv', 'w') as outf:
        reader = csv.reader(inf)
        writer = csv.writer(outf)
        line_count = 0
        for line in reader:
            if line_count == 0:
                writer.writerow([line[0], line[1], line[2], line[3], line[4], line[5]])
            else:
                imgNum = int(line[0])
                finalImgNum = imgNum + currentNum
                imgfilename = "E:\\finalheadboundingbox\\HeadBoundingBoxDataFinal\\test_imgs\\" + line[0] + ".png"
                destfilename = "E:\\finalheadboundingbox\\HeadBoundingBoxDataFinal\\test_imgs_final\\" + str(finalImgNum).zfill(8) + ".png"
                writer.writerow([str(finalImgNum).zfill(8) + ".png", line[1], line[2], line[3], line[4], line[5]])
                print(destfilename)
                copyfile(imgfilename, destfilename)
            line_count += 1

            # if line[1] == '0':
            #     writer.writerow([line[0], '1')
            #     break
            # else:
            #     writer.writerow(line)
            #     writer.writerows(reader)
    # os.remove('path/to/filename')
    # os.rename('path/to/filename_temp', 'path/to/filename')

def addPngToCSV():
    with open('E:\\finalheadboundingbox\\HeadBoundingBoxDataFinal\\head_bounding_box_train_final.csv') as inf, open('E:\\finalheadboundingbox\\HeadBoundingBoxDataFinal\\head_bounding_box_train_final_out.csv', 'w') as outf:
        reader = csv.reader(inf)
        writer = csv.writer(outf)
        line_count = 0
        for line in reader:
            if line_count == 0:
                writer.writerow([line[0], line[1], line[2], line[3], line[4], line[5]])
            else:
                print(line[0] + ".png")
                writer.writerow([line[0] + ".png", line[1], line[2], line[3], line[4], line[5]])
            line_count += 1

### NO ALPHAS, NO GRAYSCALES
def transformImagesToRGBs():
    for root, dirs, files in os.walk("D:\\GazeDatasets30K\\test"):
        for file in files:
            if file.endswith(".png"):
                fulltextfilename = os.path.join(root, file)
                imgfilename = fulltextfilename[-12:]
                convertImgToNongrayscale(fulltextfilename, HEAD_BOUNDINGBOX_OUTPUT + imgfilename)
                print(fulltextfilename)

def mergeSyntheticAndRealDataset():
    ### Train Set
    prevImgTrain = ""
    prevImgTest = ""
    csvData_train = [['imgnum', 'xmin', 'ymin', 'xmax', 'ymax', 'hasHead']]
    csvData_test = [['imgnum', 'xmin', 'ymin', 'xmax', 'ymax', 'hasHead']]
    train_num = -1
    test_num = -1
    max_test_synthdata_num = 140400
    max_test_realdata_num = 1960
    with open('E:\\GazeDatasetsOutput\\head_bounding_box_final.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                pass
            else:
                subdir = row[0]
                xmin = float(row[1])
                ymin = float(row[2])
                xmax = float(row[3])
                ymax = float(row[4])
                isHead = int(row[5])
                if line_count < max_test_synthdata_num:
                    if prevImgTrain != subdir:
                        train_num += 1
                        subdir = subdir.replace('/', "\\")
                        fullimgfilename = OUTPUT_DIR + "images\\" + subdir + ".png"
                        dstimg = MERGED_DATASET_OUTPUT + "train_imgs\\" + str(train_num).zfill(8) + ".png"
                        copyfile(fullimgfilename, dstimg)
                        rowdata = [str(train_num).zfill(8) + ".png", str(xmin), str(ymin), str(xmax), str(ymax), str(isHead)]
                        print(rowdata)
                        csvData_train.append(rowdata)
                        prevImgTrain = row[0]
                    else:
                        subdir = subdir.replace('/', "\\")
                        dstimg = MERGED_DATASET_OUTPUT + "train_imgs\\" + str(train_num).zfill(8) + ".png"
                        rowdata = [str(train_num).zfill(8) + ".png", str(xmin), str(ymin), str(xmax), str(ymax), str(isHead)]
                        print(rowdata)
                        csvData_train.append(rowdata)
                else:
                    if prevImgTest != subdir:
                        test_num += 1
                        subdir = subdir.replace('/', "\\")
                        fullimgfilename = OUTPUT_DIR + "images\\" + subdir + ".png"
                        dstimg = MERGED_DATASET_OUTPUT + "test_imgs\\" + str(test_num).zfill(8) + ".png"
                        copyfile(fullimgfilename, dstimg)
                        rowdata = [str(test_num).zfill(8) + ".png", str(xmin), str(ymin), str(xmax), str(ymax), str(isHead)]
                        print(rowdata)
                        csvData_test.append(rowdata)
                        prevImgTest = row[0]
                    else:
                        subdir = subdir.replace('/', "\\")
                        dstimg = MERGED_DATASET_OUTPUT + "test_imgs\\" + str(test_num).zfill(8) + ".png"
                        rowdata = [str(test_num).zfill(8) + ".png", str(xmin), str(ymin), str(xmax), str(ymax), str(isHead)]
                        print(rowdata)
                        csvData_test.append(rowdata)
            line_count += 1

    prevImgTrain = ""
    prevImgTest = ""
    with open('E:\\FDDBDatasetNew\\head_bounding_box_cleaned_resized_final.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                pass
            else:
                subdir = row[0]
                xmin = float(row[1])
                ymin = float(row[2])
                xmax = float(row[3])
                ymax = float(row[4])
                isHead = int(row[5])
                if line_count < max_test_realdata_num:
                    if prevImgTrain != subdir:
                        train_num += 1
                        subdir = subdir.replace('/', "\\")
                        fullimgfilename = FDDBDATASET_ROOT + "images2\\" + subdir + ".png"
                        dstimg = MERGED_DATASET_OUTPUT + "train_imgs\\" + str(train_num).zfill(8) + ".png"
                        copyfile(fullimgfilename, dstimg)
                        rowdata = [str(train_num).zfill(8) + ".png", str(xmin), str(ymin), str(xmax), str(ymax), str(isHead)]
                        print(rowdata)
                        csvData_train.append(rowdata)
                        prevImgTrain = row[0]
                    else:
                        subdir = subdir.replace('/', "\\")
                        dstimg = MERGED_DATASET_OUTPUT + "train_imgs\\" + str(train_num).zfill(8) + ".png"
                        rowdata = [str(train_num).zfill(8) + ".png", str(xmin), str(ymin), str(xmax), str(ymax), str(isHead)]
                        print(rowdata)
                        csvData_train.append(rowdata)
                else:
                    if prevImgTest != subdir:
                        test_num += 1
                        subdir = subdir.replace('/', "\\")
                        fullimgfilename = FDDBDATASET_ROOT + "images2\\" + subdir + ".png"
                        dstimg = MERGED_DATASET_OUTPUT + "test_imgs\\" + str(test_num).zfill(8) + ".png"
                        copyfile(fullimgfilename, dstimg)
                        rowdata = [str(test_num).zfill(8) + ".png", str(xmin), str(ymin), str(xmax), str(ymax), str(isHead)]
                        print(rowdata)
                        csvData_test.append(rowdata)
                        prevImgTest = row[0]
                    else:
                        subdir = subdir.replace('/', "\\")
                        dstimg = MERGED_DATASET_OUTPUT + "test_imgs\\" + str(test_num).zfill(8) + ".png"
                        rowdata = [str(test_num).zfill(8) + ".png", str(xmin), str(ymin), str(xmax), str(ymax), str(isHead)]
                        print(rowdata)
                        csvData_test.append(rowdata)
            line_count += 1

    with open(MERGED_DATASET_OUTPUT + 'head_bounding_box_train.csv', 'w', newline='') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerows(csvData_train)
    csvFile.close()
    with open(MERGED_DATASET_OUTPUT + 'head_bounding_box_test.csv', 'w', newline='') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerows(csvData_test)
    csvFile.close()

def cleanCSVFilesTwo():
    # imgnum,xmin,xmax,ymin,ymax,hasHead
    with open('E:\\WIDER FACE\\mergeddatasets_val_final.csv') as inf, open('E:\\WIDER FACE\\mergeddatasets_val_final_final.csv', 'w') as outf:
        reader = csv.reader(inf)
        writer = csv.writer(outf)
        line_count = 0

        for line in reader:
            # [['imgnum', 'xmin', 'ymin', 'xmax', 'ymax', 'hasHead']]
            if line_count == 0:
                writer.writerow([line[0], line[1], line[2], line[3], line[4], line[5]])
            else:
                imgFilename = line[0]
                xmin = int(float(line[1]))
                xmax = int(float(line[2]))
                ymin = int(float(line[3]))
                ymax = int(float(line[4]))
                hasHead = int(line[5])
                if (hasHead == 1):
                    writer.writerow([line[0], str(xmin), str(xmax), str(ymin), str(ymax), str(hasHead)])
            line_count += 1

def divideToTrainAndTestImages():
    prevImgTrain = ""
    prevImgTest = ""
    csvData_train = [['imgnum', 'xmin', 'xmax', 'ymin', 'ymax', 'hasHead']]
    csvData_test = [['imgnum', 'xmin', 'xmax', 'ymin', 'ymax', 'hasHead']]
    with open('D:\\GazeDatasets30K\\head_bounding_box.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                pass
            else:
                filenamerow = row[0]
                imgnumrow = row[1]
                xmin = float(row[2])
                xmax = float(row[3])
                ymin = float(row[4])
                ymax = float(row[5])
                isHead = int(row[6])
                rowdata = [imgnumrow + ".png", str(xmin), str(xmax), str(ymin), str(ymax), str(1)]
                print(rowdata)
                if line_count <= 28800:
                    csvData_train.append(rowdata)
                    dstimg = OUTPUT_DIR + "train\\" + imgnumrow + ".png"
                else:
                    csvData_test.append(rowdata)
                    dstimg = OUTPUT_DIR + "test\\" + imgnumrow + ".png"
                srcimg = "D:\\GazeDatasets30K\\images\\" + imgnumrow + ".png"
                copyfile(srcimg, dstimg)
            line_count = line_count + 1
    with open(OUTPUT_DIR + 'train.csv', 'w', newline='') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerows(csvData_train)
    csvFile.close()
    with open(OUTPUT_DIR + 'test.csv', 'w', newline='') as csvFile2:
        writer2 = csv.writer(csvFile2)
        writer2.writerows(csvData_test)
    csvFile2.close()

def convFloatToNum():
    csvData = [['imgnum', 'xmin', 'xmax', 'ymin', 'ymax', 'hasHead']]
    with open('D:\\GazeDatasets30K\\test.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                pass
            else:
                imgnum = row[0]
                xmin = int(float(row[1]))
                xmax = int(float(row[2]))
                ymin = int(float(row[3]))
                ymax = int(float(row[4]))
                isHead = int(row[5])
                rowdata = [imgnum, str(xmin), str(xmax), str(ymin), str(ymax), str(1)]
                print(rowdata)
                csvData.append(rowdata)
            line_count = line_count + 1
    with open(OUTPUT_DIR + 'test_fixed.csv', 'w', newline='') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerows(csvData)
    csvFile.close()
def processMinBoundedCircle():
    img = cv2.imread('E:\\NewSamsungProjRepo\\ObjectOfInteresDatasetsSept14\\Human3\\cam3\\1-1-itemwholebinmsk.png', cv2.IMREAD_GRAYSCALE)
    ret,thresh_img = cv2.threshold(img,127,255,cv2.THRESH_BINARY)
    # print(thresh_img.compressed())
    cv2.imwrite('binary_image.png', thresh_img)

    cv2.imshow('binary_image.png', thresh_img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
def createMiniBoundingBoxImg():
    for root, dirs, files in os.walk(OBJECT_INTEREST_DATAROOT):
        for file in files:
            if file.endswith('rgb.png'):
                fullimgfilename = os.path.join(root, file)
                print(fullimgfilename)
                fullfilepath = fullimgfilename[0:fullimgfilename.rfind('\\')] + "\\"
                filenumname = fullimgfilename[fullimgfilename.rindex('\\')+1:][:-7]
                itemwholefileimg = fullfilepath + filenumname + "itemwholebinmsk.png"
                jsonfilename = fullfilepath + filenumname + "final.json"
                img_color = cv2.imread(fullimgfilename, cv2.IMREAD_COLOR)
                img = cv2.imread(itemwholefileimg, cv2.IMREAD_GRAYSCALE)
                ret, thresh_img = cv2.threshold(img,127,255,cv2.THRESH_BINARY)
                segm_points = []
                (height, width) = img.shape
                blank_image = np.zeros((height,width,3), np.uint8)
                for x in range(width):
                    for y in range(height):
                        # Point part of the item segmentation, equals 1
                        if (thresh_img[y, x] == 255):
                            segm_points.append([y, x])
                            # blank_image[y, x] = img_color[y, x]
                minBoundingCircle = cv2.minEnclosingCircle(np.array(segm_points))
                (circle_y, circle_x) = minBoundingCircle[0]
                circle_radius = minBoundingCircle[1]
                xmin = int(circle_x - circle_radius) # In integer piel coordinates
                ymin = int(circle_y - circle_radius)
                xmax = int(circle_x + circle_radius)
                ymax = int(circle_y + circle_radius)
                for pos_x in range(xmin, xmax+1):
                    for pos_y in range(ymin, ymax+1):
                        blank_image[pos_y, pos_x] = img_color[pos_y, pos_x]
                # cv2.circle(blank_image,(int(circle_x), int(circle_y)), int(circle_radius), (0,255,0), 1)
                with open(jsonfilename) as f:
                    data_dict = json.load(f)
                data_dict["xmin_itemcirclebbox"] = xmin
                data_dict["ymin_itemcirclebbox"] = ymin
                data_dict["xmax_itemcirclebbox"] = xmax
                data_dict["ymax_itemcirclebbox"] = ymax
                data_dict["item_circleX"] = circle_x # Still floating point
                data_dict["item_circleY"] = circle_y
                data_dict["item_circleRad"] = circle_radius
                with open(jsonfilename, 'w+') as fp: # overwrite json
                    json.dump(data_dict, fp)
                itemGazeCircleBBoxImgname = fullfilepath + filenumname + "mincircleBBoxRGB.png"
                cv2.imwrite(itemGazeCircleBBoxImgname, blank_image)
                # pixels = im.load()
                # newimg = Image.new('RGB', im.size)
                # pixelsNew = newimg.load()
                #
def collectObjectOfInterestDirs():
    csvData_train = [['imgfile', 'xmin', 'ymin', 'xmax', 'ymax', 'category_id']]
    csvData_test = [['imgfile', 'xmin', 'ymin', 'xmax', 'ymax', 'category_id']]
    csvData_val = [['imgfile', 'xmin', 'ymin', 'xmax', 'ymax', 'category_id']]
    for root, dirs, files in os.walk(OBJECT_INTEREST_DATAROOT):
        for file in files:
            if file.endswith('rgb.png'):
                fullimgfilename = os.path.join(root, file)
                print(fullimgfilename)
                fullfilepath = fullimgfilename[0:fullimgfilename.rfind('/')] + "/"
                filenumname = fullimgfilename[fullimgfilename.rindex('/')+1:][:-7]
                humancam = fullfilepath + filenumname
                humancam = humancam[len(OBJECT_INTEREST_DATAROOT):]
                jsonfilename = fullfilepath + filenumname + "final.json"
                with open(jsonfilename) as json_file:
                    data_dict = json.load(json_file)
                xmin_itembb = int(data_dict["xmin_itembb"])
                ymin_itembb = int(data_dict["ymin_itembb"])
                xmax_itembb = int(data_dict["xmax_itembb"])
                ymax_itembb = int(data_dict["ymax_itembb"])
                category_id = 1
                if ("1-10" in humancam) or ("2-10" in humancam) or ("3-10" in humancam):
                    csvData_test.append([humancam, str(xmin_itembb), str(ymin_itembb), str(xmax_itembb), str(ymax_itembb), str(category_id)])
                elif ("1-9" in humancam) or ("2-9" in humancam) or ("3-9" in humancam):
                    csvData_val.append([humancam, str(xmin_itembb), str(ymin_itembb), str(xmax_itembb), str(ymax_itembb), str(category_id)])
                else:
                    csvData_train.append([humancam, str(xmin_itembb), str(ymin_itembb), str(xmax_itembb), str(ymax_itembb), str(category_id)])
    with open(OBJECT_INTEREST_DATAROOT + 'train.csv', 'w', newline='') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerows(csvData_train)
    csvFile.close()
    with open(OBJECT_INTEREST_DATAROOT + 'test.csv', 'w', newline='') as csvFile2:
        writer = csv.writer(csvFile2)
        writer.writerows(csvData_test)
    csvFile2.close()
    with open(OBJECT_INTEREST_DATAROOT + 'val.csv', 'w', newline='') as csvFile3:
        writer = csv.writer(csvFile3)
        writer.writerows(csvData_val)
    csvFile3.close()

def divideObjectOfInterestDataIntoTrainValidTest():
    csvData_train = [['imgfile', 'jsonfile']]
    csvData_test = [['imgfile', 'jsonfile']]
    csvData_val = [['imgfile', 'jsonfile']]
    print(OBJECT_INTEREST_DATAROOT)
    for root, dirs, files in os.walk(OBJECT_INTEREST_DATAROOT):
        for file in files:
            if file.endswith('rgb.png'):
                fullimgfilename = os.path.join(root, file)
                print(fullimgfilename)
                fullfilepath = fullimgfilename[0:fullimgfilename.rfind('/')] + "/"
                filenumname = fullimgfilename[fullimgfilename.rindex('/')+1:][:-7]
                jsonfilename = fullfilepath + filenumname + "final.json"
                humandir = fullfilepath + filenumname
                with open(jsonfilename) as json_file:
                    data_dict = json.load(json_file)
                itemNum = int(data_dict["itemNum"])
                humanNum = int(re.search('/Human(.*)/cam', jsonfilename).group(1))
                humandir = humandir[len(OBJECT_INTEREST_DATAROOT):]
                imgfileData = humandir + "rgb.png"
                jsonfileData = humandir + "final.json"
                # print("humandir: ", humandir)
                if (itemNum == 4): ## Use item 4 for valid and test set
                    if (humanNum >= 1 and humanNum <= 25):
                        csvData_val.append([imgfileData, jsonfileData])
                    else:
                        csvData_test.append([imgfileData, jsonfileData])
                else:
                    csvData_train.append([imgfileData, jsonfileData])
    with open(OBJECT_INTEREST_DATAROOT + 'train_goo.csv', 'w', newline='') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerows(csvData_train)
    csvFile.close()
    with open(OBJECT_INTEREST_DATAROOT + 'test_goo.csv', 'w', newline='') as csvFile2:
        writer = csv.writer(csvFile2)
        writer.writerows(csvData_test)
    csvFile2.close()
    with open(OBJECT_INTEREST_DATAROOT + 'val_goo.csv', 'w', newline='') as csvFile3:
        writer = csv.writer(csvFile3)
        writer.writerows(csvData_val)
    csvFile3.close()

def createItemClassPermuteStr():
    

if __name__ == "__main__":
    ### SINGLE FILES
    # preprocessquats_singleFile("E:\\test\\2.txt")
    # saveSingleBoundingBoxData("E:\\test\\1-binmsk.png", "E:\\test\\1-bbox.txt")

    ### MULTIPLE BULKED FILES
    # preprocessDataset()
    # preprocessrgbs()
    # createBoundingBox()
    # getXYHeadLocation()
    # testBBoxCoords()
    # fixBBoxImages()
    # refixBBoxAnnots()
    # fixBBoxImages()
    # refixSegmFiles()
    # createSegmLocAndFinalInfoTxtFile()
    # processMinBoundedCircle()
    # createMiniBoundingBoxImg()
    # collectObjectOfInterestDirs()

    ### FDDB Dataset preprocess
    # preprocessFDDBDataset()
    # cleanFDDBDataset()

    # transformImagesToRGBs()
    # renameImagesInFolder()
    # addPngToCSV()
    # cleanCSVFilesTwo()
    # removeAllNewLinesCSV()
    # convFloatToNum()

    ### WIDER FACE DATASET
    # preprocessWiderFaceDataset()

    ### SAMPLE AND PREPARE GAZE DATASET
    # sampleFromImagesFromGazeDataset()
    # divideToTrainAndTestImages()
    # divideObjectOfInterestDataIntoTrainValidTest()

    ## MERGE SYNTHETIC AND REAL GazeDatasets
    # mergeSyntheticAndRealDataset()

    ## PREPARE GAZE OBJECT DATASET
    # collectObjectOfInterestDirs()
    checkIfPointInsideCone()
